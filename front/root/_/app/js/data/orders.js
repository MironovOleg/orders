////////////////////////////////////////////////////////////////////////////////

$_GET.orders = async function (o) {

    let data = await response ({type: 'orders', part: 'vocs'})

    add_vocabularies (data, {})
    
    $('body').data ('data', data)
            
    return data

}