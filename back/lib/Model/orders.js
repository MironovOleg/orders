module.exports = {

    label : 'Договора',

    columns : {
        uuid         : 'uuid=uuid_generate_v4()                     // "Идентификатор" договора',
        is_deleted   : 'int=0                                       // "статус" договора',
        number       : 'string                                      // "номер" договора',
        date_begin   : 'string                                      // "Дата начала действия" договора',
        date_end     : 'string                                      // "Дата окончания действия" договора',
    },

    pk: 'uuid',

    keys : {
        login    : 'UNIQUE (uuid)',
    },

}