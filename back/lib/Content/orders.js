const Dia = require ('../Ext/Dia/Dia.js')

module.exports = {

////////////////////////////////////////////////////////////////////////////////

get_vocs_of_orders:

    function () {

        return this.db.add_vocabularies ({_fields: this.db.model.tables.orders.columns}, {
        })

    },
    
////////////////////////////////////////////////////////////////////////////////

select_orders:
    
    function () {
   
        this.rq.sort = this.rq.sort || [{field: "number", direction: "asc"}]
        //
        // if (this.rq.searchLogic == 'OR') {
        //
        //     let q = this.rq.search [0].value
        //
        //     this.rq.search = [
        //         {field: 'label', operator: 'contains', value: q},
        //         {field: 'login', operator: 'contains', value: q},
        //         {field: 'mail',  operator: 'contains', value: q},
        //     ]
        //
        // }
    
        let filter = this.w2ui_filter ()
        
        filter.is_deleted  = 0

        return this.db.add_all_cnt ({}, [{orders: filter}])

    },

////////////////////////////////////////////////////////////////////////////////
    
get_item_of_orders:

    async function () {
        
        let data = await this.db.get ([{orders:

            {uuid: this.rq.id},

        }])
        
        data._fields = this.db.model.tables.orders.columns
        
        return data

    },

do_delete_orders:

    async function () {
    
        this.db.update ('orders', {
            uuid        : this.rq.id,
            is_deleted  : 1,
        })

    },

////////////////////////////////////////////////////////////////////////////////

do_update_orders:

    async function () {
    
        let data = this.rq.data

        let uuid = this.rq.id

        let d = {uuid}

        for (let k of ['number', 'date_begin', 'date_end']) d [k] = data [k]
        
        try {
            await this.db.update ('orders', d)
        }
        catch (x) {
            throw x || "Ошибка"
        }

    },

////////////////////////////////////////////////////////////////////////////////

do_create_orders:

    async function () {
    
        let data = this.rq.data

        let d = {uuid: this.rq.id}

        for (let k of ['number', 'date_begin', 'date_end']) d [k] = data [k]
        
        try {
            await this.db.insert ('orders', d)
        }
        catch (x) {
            if (this.db.is_pk_violation (x)) return d

            throw x || "Ошибка"
        }
        
        return d

    },

}