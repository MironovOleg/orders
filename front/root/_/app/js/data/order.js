////////////////////////////////////////////////////////////////////////////////

$_DO.cancel_order = function (e) {
    
    if (confirm ('Отменить несохранённые правки?')) w2_panel_form ().reload_block ()

}

////////////////////////////////////////////////////////////////////////////////

$_DO.edit_order = function (e) {

	$_SESSION.delete ('__read_only')
	
	w2_panel_form ().refresh ()

}

////////////////////////////////////////////////////////////////////////////////

$_DO.delete_order = async function (e) {
    
    if (!confirm ('Вы уверены?')) return

    let form = w2_popup_form ()

    await response ({type: 'orders', action: 'delete'}, {
        id: form.original.uuid
    })

    w2_close_popup_reload_grid ()

    refreshOpener ()

}

////////////////////////////////////////////////////////////////////////////////

$_DO.update_order = async function (e) {

    if (!confirm ('Сохранить изменения?')) return

    let form = w2_popup_form ()

    let data = form.values ().actual ().validated ()

    form.lock ()

    await response ({type: 'orders', action: 'update'}, {data, id: form.original.uuid})

    w2_close_popup_reload_grid ()

}

////////////////////////////////////////////////////////////////////////////////

$_GET.order = async function (o) {

    const query = {type: 'orders'};
    if(o && o.id){
        query.id =o.id;
    }
    let data = await response (query)
    
    $('body').data ('data', data)

    return data

}