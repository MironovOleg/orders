$_DRAW.orders = async function (data) {

    $('title').text ('Реестр договоров')

    $('main').w2regrid ({ 
    
        name: 'ordersGrid',
        
        show: {
            toolbar: true,
            footer: true,
            toolbarAdd: true,
        },            

        columns: [                
            {field: 'number',   caption: 'Номер',    size: 100, sortable: true},
            {field: 'date_begin',   caption: 'Дата начала',    size: 100, sortable: true},
            {field: 'date_end',   caption: 'Дата конца',    size: 100, sortable: true},
        ],
                    
        url: '_back/?type=orders',

        onAdd:      ( ) => show_block ('order_new'),
        onDblClick: (e) => show_block   (`order`, {
            id: e.recid
        }),

    }).refresh ();
    
    $('#grid_ordersGrid_search_all').focus ()

}